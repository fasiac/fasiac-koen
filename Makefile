RELEASE = 0.2.0

.PHONY: usage apt-cacher-ng build remove run copy all default

usage:
	@echo "Please provide an option:"
	@echo " make build  --- Build the FASiAC KOEN ISO and grub2 builder"
	@echo " make grub2  --- Run a container to build grub2 .deb packages"
	@echo " make koen   --- Run a container to build the FASiAC KOEN ISO image"
	@echo " make all    --- Execute apt-cacher-ng, koen-builder, koen"

apt-cacher-ng:
	test -d $(PWD)/var/cache/apt-cacher-ng || mkdir -p $(PWD)/var/cache/apt-cacher-ng
	RELEASE=$(RELEASE) docker-compose up -d fasiac-koen-apt-cacher-ng

koen-builder:
	RELEASE=$(RELEASE) docker-compose build fasiac-koen-builder

grub2-builder:
	RELEASE=$(RELEASE) docker-compose build grub2-builder

grub2:
	RELEASE=$(RELEASE) docker-compose run grub2-builder

koen:
	RELEASE=$(RELEASE) docker-compose run fasiac-koen-builder

all: apt-cacher-ng koen-builder koen

default: usage
