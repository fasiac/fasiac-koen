#!/bin/sh
# Main script for ISO image creation

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

# shellcheck disable=SC1091
. ./variables.sh

if test -x "checksum.sh"
then
  # shellcheck disable=SC1091
  . ./checksum.sh
else
  SHA512SUM="$(sha512sum < "/dev/cdrom")"
fi

# Create working directories
mkdir -p "${WD}"
CHROOT="${WD}/chroot"
IMAGE="${WD}/image"
LIVE="${IMAGE}/live"
SCRATCH="/tmp/scratch"

# Set up the base Debian rootfs environment
debuerreotype-init "${CHROOT:?}" "${DIST}" "${DATE}" --arch="${ARCH}"

# Set network
echo "fasiac-koen" > "${CHROOT:?}/etc/hostname"

cat > "${CHROOT}/etc/hosts" << EOF
127.0.0.1       localhost fasiac-koen
EOF

# Check that apt-cacher-ng proxy is reachable
ping -qc1 fasiac-koen-apt-cacher-ng \
  | head -n1 \
  | sed -e 's/[^(]*(\([^)]*\)).*/\1 fasiac-koen-apt-cacher-ng/' \
  >> "${CHROOT}/etc/hosts"

# Copy ressources for reproduction
for ressource in README.md LICENSE.md create-iso.sh variables.sh SHA512SUMS tools/ ca-tools/
do
  cp -a "${ressource}" "${CHROOT:?}/${ressource}"
  chown -R 0:0 "${CHROOT:?}/${ressource}"
done
mv "${CHROOT:?}/ca-tools/opt" "${CHROOT:?}/"
mv "${CHROOT:?}/ca-tools/etc/profile.d" "${CHROOT:?}/etc/"

# Enable root without password
debuerreotype-chroot "${CHROOT:?}" passwd -d root
# Install all needed packages for fasiac-koen
debuerreotype-apt-get "${CHROOT:?}" update
# Explicitly call apt-get to be able to pass in options
debuerreotype-chroot "${CHROOT:?}" \
    DEBIAN_FRONTEND=noninteractive \
    apt-get \
    -o Acquire::Check-Valid-Until=false \
    install \
    --no-install-recommends --yes \
    linux-image-amd64 live-boot systemd-sysv \
    iproute2 ifupdown pciutils usbutils dosfstools eject exfat-utils \
    vim links2 xpdf cups cups-bsd enscript libbsd-dev tree less iputils-ping \
    policykit-1 \
    xserver-xorg-core xserver-xorg x11-xserver-utils \
    xfce4 xfce4-terminal xfce4-panel xfce4-battery-plugin \
    xfce4-power-manager xfce4-power-manager-data xfce4-power-manager-plugins \
    lightdm system-config-printer \
    xterm gvfs thunar-volman \
    openssl libengine-pkcs11-openssl1.1 \
    libccid pcscd \
    opensc opensc-pkcs11 nitrokey-app \
    python3-minimal \
    mtools efibootmgr gettext-base libfuse2 \
    patch liblzo2-2 xorriso debootstrap apt-cacher-ng
debuerreotype-chroot "${CHROOT:?}" \
    dpkg -i /tools/packages/squashfs-tools_4.3-8.0tails1_amd64.deb
debuerreotype-chroot "${CHROOT:?}" \
    dpkg -i /tools/packages/debuerreotype_0.7-1_all.deb
debuerreotype-apt-get "${CHROOT:?}" --yes --purge autoremove
debuerreotype-apt-get "${CHROOT:?}" --yes clean

# Apply hooks
for FIXES in "${HOOK_DIR}"/*.sh
do
  $FIXES
done

# Add udev rule for nitrokey
cp -a /tools/41-nitrokey.rules "${CHROOT:?}/etc/udev/rules.d/"

# Configure a no-network setup
cat > "${CHROOT:?}/etc/network/interfaces.d/no-network" << EOF
auto lo
iface lo inet loopback
EOF

# Profile in .bashrc to work with xfce terminal
# ls with color
sed -i -r -e '9s/^#//' \
          -e '10s/^#//' \
          -e '11s/^#//' \
    "${CHROOT:?}/root/.bashrc"

# Configure autologin
for NUMBER in $(seq 1 6)
do
      mkdir -p "${CHROOT:?}/etc/systemd/system/getty@tty${NUMBER}.service.d"
cat > "${CHROOT:?}/etc/systemd/system/getty@tty${NUMBER}.service.d/live-config_autologin.conf" << EOF
[Service]
Type=idle
ExecStart=
ExecStart=-/sbin/agetty --autologin root --noclear %I \$TERM
TTYVTDisallocate=no
EOF
done

# Enable XFCE root auto login
sed -i -r -e "s|^#.*autologin-user=.*\$|autologin-user=root|" \
          -e "s|^#.*autologin-user-timeout=.*\$|autologin-user-timeout=0|" \
    "${CHROOT:?}/etc/lightdm/lightdm.conf"

sed -i --regexp-extended \
    '11s/.*/#&/' \
    "${CHROOT:?}/etc/pam.d/lightdm-autologin"

# Disable lastlog since autologin is enabled
sed -i '/^[^#].*pam_lastlog\.so/s/^/# /' "${CHROOT:?}/etc/pam.d/login"

# Make sure that xscreensaver is off
rm -f "${CHROOT:?}/etc/xdg/autostart/xscreensaver.desktop"

# Define mount point /media/ for HSMFD, HSMFD1 and KSRFD
cat > "${CHROOT:?}/etc/udev/rules.d/99-udisks2.rules" << EOF
# UDISKS_FILESYSTEM_SHARED
# ==1: mount filesystem to a shared directory (/media/VolumeName)
# ==0: mount filesystem to a private directory (/run/media/USER/VolumeName)
# See udisks(8)
ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
EOF

# Create boot directories
mkdir -p "${LIVE}"

# Copy bootloader
cp -p "${CHROOT:?}/boot"/vmlinuz-* "${LIVE}/vmlinuz"
cp -p "${CHROOT:?}/boot"/initrd.img-* "${LIVE}/initrd.img"

# Copy open-sc specific files
mkdir -p "${CHROOT:?}/root/.cache/"
cp -a "${CHROOT:?}/ca-tools/smartcard_list.txt" "${CHROOT:?}/root/.cache/"
chown -R 0:0 "${CHROOT:?}/root/"

# Copy apt-cacher-ng cache
# TODO: cp -a /var/cache/apt-cacher-ng "${CHROOT}:/var/cache/apt-cacher-ng"

# Remove non-reproducible apt-cacher-ng files (random order / unsorted)
rm "${CHROOT:?}"/var/lib/apt-cacher-ng/backends*
rm "${CHROOT:?}"/etc/apt-cacher-ng/backends*
echo "http://ftp.debian.org/debian/" \
  > "${CHROOT:?}"/etc/apt-cacher-ng/backends_debian

# Create minimal apt-cacher-ng config
cat > "${CHROOT:?}"/etc/apt-cacher-ng/acng.conf <<EOF
#
# IMPORTANT NOTE:
#
# THIS FILE IS MAYBE JUST ONE OF MANY CONFIGURATION FILES IN THIS DIRECTORY.
# SETTINGS MADE IN OTHER FILES CAN OVERRIDE VALUES THAT YOU CHANGE HERE. GO
# LOOK FOR OTHER CONFIGURATION FILES! CHECK THE MANUAL AND INSTALLATION NOTES
# (like README.Debian) FOR MORE DETAILS!
#

# This is a configuration file for apt-cacher-ng, a smart caching proxy for
# software package downloads. It's supposed to be in a directory specified by
# the -c option of apt-cacher-ng, see apt-cacher-ng(8) for details.
CacheDir: /var/cache/apt-cacher-ng
LogDir: /var/log/apt-cacher-ng
SupportDir: /usr/lib/apt-cacher-ng

# Repository remapping. See manual for details.
# No remapping wanted.

# Days before considering an unreferenced file expired (to be deleted).
# WARNING: if the value is set too low and particular index files are not
# available for some days (mirror downtime) then there is a risk of removal of
# still useful package files.
#
ExThreshold: 4

# Precache a set of files referenced by specified index files. This can be used
# to create a partial mirror usable for offline work. There are certain limits
# and restrictions on the path specification, see manual and the cache control
# web site for details. A list of (maybe) relevant index files could be
# retrieved via "apt-get --print-uris update" on a client machine.
#
# Example:
# PrecacheFor: debrep/dists/unstable/*/source/Sources* debrep/dists/unstable/*/binary-amd64/Packages*

# ResponseFreezeDetectTime: 500

# Path to the system directory containing trusted CA certificates used for
# outgoing connections, see OpenSSL documentation for details.
#
# CApath: /etc/ssl/certs
#
# Path to a single trusted trusted CA certificate used for outgoing
# connections, see OpenSSL documentation for details.
#
# CAfile:
EOF

# Make localhost apt-cacher-ng proxy for live ISO
cat > "${CHROOT:?}/etc/hosts" << EOF
127.0.0.1       localhost fasiac-koen fasiac-koen-apt-cacher-ng
EOF

# Create boot stuff
mkdir -p "${CHROOT:?}/${SCRATCH:?}"
cat <<EOF > "${CHROOT:?}/${SCRATCH}/grub.cfg"

search --set=root --file /FASIAC_KOEN_BOOT_ID

insmod all_video

set timeout=0
set default="0"

menuentry "FASIaC Key Operation Environment" {
  linux /live/vmlinuz boot=live quiet nomodeset
  initrd /live/initrd.img
}
EOF
touch "${IMAGE}/FASIAC_KOEN_BOOT_ID"

mkdir -p "${CHROOT:?}/${SCRATCH:?}/efi/boot"
debuerreotype-chroot "${CHROOT:?}" \
grub-mkstandalone \
  -t "${SOURCE_DATE_EPOCH}" \
  --format=x86_64-efi \
  --output="${SCRATCH:?}/efi/boot/bootx64.efi" \
  --locales="" \
  --fonts="" \
  "boot/grub/grub.cfg=${SCRATCH:?}/grub.cfg"
find "${CHROOT:?}/${SCRATCH:?}/efi" \
  -exec touch \
  --no-dereference \
  --date="@${SOURCE_DATE_EPOCH}" \
  '{}' +

cat <<EOF > "${CHROOT:?}/${SCRATCH:?}/mkefiboot.sh"
#!/bin/sh
  cd "${SCRATCH:?}"
  dd if=/dev/zero of=efiboot.img bs=1M count=10
  # note that you must use --invariant before -i
  # --invariant is the only facility for a fixed creation date
  mkfs.vfat --invariant -i 40e2b007 -n "FASIAC_KOEN" efiboot.img
  mcopy -smp -i efiboot.img ./efi ::
EOF
chmod a+x "${CHROOT:?}/${SCRATCH:?}/mkefiboot.sh"

debuerreotype-chroot "${CHROOT:?}" \
  "${SCRATCH:?}/mkefiboot.sh"

debuerreotype-chroot "${CHROOT:?}" \
grub-mkstandalone \
  -t "${SOURCE_DATE_EPOCH}" \
  --format=i386-pc \
  --output="${SCRATCH:?}/core.img" \
  --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
  --modules="linux normal iso9660 biosdisk search" \
  --locales="" \
  --fonts="" \
  "boot/grub/grub.cfg=${SCRATCH:?}/grub.cfg"

# Fix dates to SOURCE_DATE_EPOCH
debuerreotype-fixup "${CHROOT:?}"

# Recreate font caches
debuerreotype-chroot "${CHROOT:?}" \
  fc-cache -srf

# Fix dates to SOURCE_DATE_EPOCH again
debuerreotype-fixup "${CHROOT:?}"

# Move boot images into image (primarily as a reference for an easy review)
mkdir -p "${IMAGE:?}/boot/grub/i386-pc"
cat \
  "${CHROOT:?}/usr/lib/grub/i386-pc/cdboot.img" \
  "${CHROOT:?}/${SCRATCH:?}/core.img" \
> "${IMAGE:?}/boot/grub/bios.img"
cp "${CHROOT:?}/usr/lib/grub/i386-pc/cdboot.img" "${IMAGE:?}/boot/grub/"
cp "${CHROOT:?}/${SCRATCH:?}/core.img" "${IMAGE:?}/boot/grub/"
cp "${CHROOT:?}/usr/lib/grub/i386-pc/all_video.mod" "${IMAGE:?}/boot/grub/i386-pc/all_video.mod"
mkdir -p "${IMAGE:?}/boot/EFI"
cp -a "${CHROOT:?}/${SCRATCH:?}/efiboot.img" "${IMAGE:?}/boot/EFI/efiboot.img"

# Clean scratch
rm -rf "${CHROOT:?}/${SCRATCH:?}"

# Fix main folder timestamps to SOURCE_DATE_EPOCH
find "${WD}/" -exec touch --no-dereference --date="@${SOURCE_DATE_EPOCH}" '{}' +

# Compress the chroot environment into a squashfs
mksquashfs "${CHROOT:?}/" "${LIVE:?}/filesystem.squashfs" \
  -comp xz -Xbcj x86 -b 1024K -Xdict-size 1024K \
  -no-exports -no-fragments \
  -processors 2 \
  -wildcards \
  -ef \
  "${TOOL_DIR}/mksquashfs-excludes"

# Set permissions for squashfs.img
chmod 644 "${LIVE:?}/filesystem.squashfs"

# Fix squashfs folder timestamps to SOURCE_DATE_EPOCH
find "${IMAGE:?}" -exec touch --no-dereference --date="@${SOURCE_DATE_EPOCH}" '{}' +

# Create the ISO image
test -d "${BUILD_DIR:?}" || mkdir -p "${BUILD_DIR:?}"
TARGET="${BUILD_DIR:?}/${ISONAME:?}"
test -f "${BUILD_DIR:?}/${ISONAME:?}" && rm "${BUILD_DIR:?}/${ISONAME:?}"

# NOTE: xorriso is able to process SOURCE_DATE_EPOCH env var
xorriso \
  -outdev "${TARGET:?}" \
  -padding 0 \
  -volid FASIACKOEN \
  -compliance iso_9660_level=3 \
  -overwrite off \
  -write_type sao/dao \
  -close on \
  -pathspecs as_mkisofs \
  -map "${IMAGE:?}" / \
  -boot_image any boot_info_table=on \
  -boot_image any grub2_mbr="${CHROOT:?}/usr/lib/grub/i386-pc/boot_hybrid.img" \
  -boot_image any grub2_boot_info=on \
  -boot_image any bin_path=boot/grub/bios.img \
  -boot_image any cat_path=boot/boot.cat \
  -boot_image any emul_type=no_emulation \
  -boot_image any load_size=2048 \
  -boot_image any next \
  -boot_image any efi_path=--interval:appended_partition_2:all:: \
  -boot_image any cat_path=boot/boot.cat \
  -append_partition 2 0xef "${IMAGE:?}/boot/EFI/efiboot.img"

echo "Calculating SHA-512 HASH of the ${TARGET:?}"
NEWHASH=$(sha512sum < "${TARGET:?}")
  if test "${NEWHASH}" != "${SHA512SUM}"
  then
      echo "ERROR: SHA-512 hashes mismatched reproduction failed"
      echo "calculated hash was: ${NEWHASH}"
      echo "Please send us an issue report: https://fasiac.io"
  else
      echo "Successfully reproduced fasiac-koen-${RELEASE}"
  fi

# END
