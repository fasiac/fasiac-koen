#!/bin/sh
# Export checksum of the ISO image.

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

# ISO image SHA-512
export SHA512SUM="5a78bf8ef7b0c710f2287b54f972b5e83b05588851899f83fcaf4f0f4405e2d9d2686dfd20a54768e5a8e23dc2bd3956dcdf477aa18ed9509f01d45dbbd34221  -"
