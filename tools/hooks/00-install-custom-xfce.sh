#!/bin/sh
# Installs XFCE customisation from Debian packages

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

PKG1="ksk-xfce-custom-0.1.0coen_amd64.deb"
SHAPKG1="1a9532cec68c042bad978758fa686a5e7bd7943e0b18b4b717ca2d3d3e557553cdffaac10b5fc8682828546d5589207def61eaa0d1da702c12ff1681d0253eb8  -"

# shellcheck disable=SC2066
for PKG in "${PKG1} ${SHAPKG1}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
