#!/bin/sh
# mkinitramfs generates non-reproducible ramdisk images
# Reference https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=845034
# The initramfs-tools packages from tails fixed the above

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

PKG1="live-boot-initramfs-tools_20170112_all.deb"
SHAPKG1="b682c0463cd969e98027b99fda0dcbb7251d876f3fa3464c78e4dbb1879ae06adb55d3ba677cb2e617ffa4a4a6b43b06bb8c948c313b42bcc391bcfcf23e8b6e  -"

PKG2="live-boot_20170112_all.deb"
SHAPKG2="93ee5dd1d3cf424eca53e21311c6ed002586dcf3b435c00e986796a318d7a340c0c11156210546e0c39d3a4b491a53b720c7b6d5d716448bcbe95867a1d16437  -"

for PKG in "${PKG1} ${SHAPKG1}" "${PKG2} ${SHAPKG2}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
