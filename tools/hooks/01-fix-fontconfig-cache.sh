#!/bin/sh
# fontconfig generates non-reproducible cache files under
# /var/cache/fontconfig
# Reference https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=864082
# The fontconfig packages from tails fixed the above

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately


PKG1="fontconfig-config_2.13.1-2.0tails1_all.deb"
SHAPKG1="ee0242e9c2864fe3d9ba806ba3b641c0177813111c9c4eb4622773ecacbc7e5f33b3ec4622b916d3a422a7e94978d0b06daa87b17eb49b4211606e8d1dfb5821  -"

PKG2="libfontconfig1_2.13.1-2.0tails1_amd64.deb"
SHAPKG2="d27d9fcdfa7d22a77ef7292ee1ed279ede0e056f4386cff60befacc6cbd121f1e5f7681463ce45a33ed7a0ab2fb767ade11018b17cf1686d021ef0ff074361fc  -"

PKG3="fontconfig_2.13.1-2.0tails1_amd64.deb"
SHAPKG3="5450addbe110b8de815be2039be214a3b83827c3803ac100944b48f927d41c817fb57a8bbcc10aa99a88453e60cbf093a853e38a76ff29350f7d997a42da507a  -"

for PKG in "${PKG1} ${SHAPKG1}" "${PKG2} ${SHAPKG2}" "${PKG3} ${SHAPKG3}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
