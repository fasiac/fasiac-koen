#!/bin/sh
# gtk+3.0 immodules.cache is not reproducible
# Reference https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=872729
# and https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=875700
# The gtk+ packages from tails fixed the above

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

PKG1="libgtk-3-common_3.24.5-1.0tails1_all.deb"
SHAPKG1="9a49053312a583bf501818f1452e384f66c751ba625d08c3e77a817730071067bfac31ab059306855d10b783ae0e805a0920682d24a100f2181e71931c2ec756  -"

PKG2="libgtk-3-0_3.24.5-1.0tails1_amd64.deb"
SHAPKG2="eb43b4e53ec464163f92388916eafe114f16b1105a2cf0095be8b0c16c2ccf83dbffff66e83ea09240f67d522f9e05dce1ca717ee5087e2677a0eebee41a8b3d  -"

PKG3="gir1.2-gtk-3.0_3.24.5-1.0tails1_amd64.deb"
SHAPKG3="63ddd08f26dde88080ed5952f7255f9c2776a39a047f35d508f7fd37fb3f67444fcbdedaae000b24a624f8fd461f5779d9c8ed6744ab34512afc2984f0a98207  -"

PKG4="gtk-update-icon-cache_3.24.5-1.0tails1_amd64.deb"
SHAPKG4="995eb66b4c384e4f15ca94465de51e83e3c75f105d3c7655da9629b0323b202d1eb068770a7cbcd8124f3c2a721b8fbca887f14c2433c796268cf8eeb80ff92d  -"

for PKG in "${PKG1} ${SHAPKG1}" "${PKG2} ${SHAPKG2}" "${PKG3} ${SHAPKG3}" "${PKG4} ${SHAPKG4}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
