#!/bin/sh
# Install debuerrotype and squashfs-tools

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

PKG1="debuerreotype_0.7-1_all.deb"
SHAPKG1="3aae7ce6f508dd288b5d1e80a1e0c971f27b80c208f5c3e7d191c04260f39b6bce488e24cdfbe288b47a09c867cc5d8807149e04d7073362eb52d13c851c276e  -"

PKG2="squashfs-tools_4.3-8.0tails1_amd64.deb"
SHAPKG2="0d5c23141e31ed6a8a17cf0bd8f2e9936bda906e6cd8786c7c8c767a7c8662bbce367d7c927aaffca89976e502e0a479845e47bb8fd0e655f89df028d0e0cab3  -"

# shellcheck disable=SC2066
for PKG in "${PKG1} ${SHAPKG1}" "${PKG2} ${SHAPKG2}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
