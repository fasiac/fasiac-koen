#!/bin/sh
# grub2 generates non-reproducible boot images

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

PKG1="grub-common_2.02+dfsg1-20_amd64.deb"
SHAPKG1="0fde816f0dbb34ea869ab64b431732cae7dfafe3afc032d422c60ded7a0b932bb065ea1988bee5ec7ffd453568076dc0da9c74349b8339845cb0721ff56f6824  -"

PKG2="grub-efi-amd64-bin_2.02+dfsg1-20_amd64.deb"
SHAPKG2="6c83b5be66c414028b55c9594c0e05be18ecd0b42879f99c3c03e78cffcf55075a656f0acc67b07b701ad3553884e1ebd6d63f0d3d60e9e47518448cebf0d3f3  -"

PKG3="grub-pc-bin_2.02+dfsg1-20_amd64.deb"
SHAPKG3="0091226e0fe6394dd7354e7e5a563d49c44df7555a79fa72d2500bc1793d182626f7c91a174a42906d5a2b2d146d816d5f32ce13630ea73aacd69781c8f72c58  -"

for PKG in "${PKG1} ${SHAPKG1}" "${PKG2} ${SHAPKG2}" "${PKG3} ${SHAPKG3}"
do
	# shellcheck disable=SC2086
	set -- ${PKG} # parses variable PKG $1 name and $2 hash and $3 "-"
	cp "${PACKAGE_DIR}/${1}" "${WD}/chroot/tmp"
	echo "Calculating SHA-512 HASH of the ${1}"
	HASH=$(sha512sum < "${WD}/chroot/tmp/${1}")
		if test "${HASH}" != "${2}  ${3}"
		then
			echo "ERROR: SHA-512 hashes mismatched"
			exit 1
		fi
	debuerreotype-chroot "${WD}/chroot" dpkg -i "/tmp/${1}"
	rm -f "${WD}/chroot/tmp/${1}"
done

# END
