#!/bin/sh
# Main script for patched grub2 package creation

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

. ./variables.sh
# scratch / work dir
SCRATCH="$(mktemp -d -p /tmp grub2-build-XXXXXXXX)"
##trap "rm -rf ${SCRATCH}" EXIT

i=0
while test "$i" -lt 100
do
  test -b "/dev/loop$i" || mknod "/dev/loop$i" -m0660 b 7 "$i"
  i=$((i+1))
done

test -d "${BUILD_DIR}" || mkdir -p "${BUILD_DIR}"

(
cd "${SCRATCH}"
apt-src install \
  grub2
)

#apt-get build-dep grub2
VERSION="$(cd "${SCRATCH}" ; apt-src version --upstream-version grub2)"

SRC="${SCRATCH}/grub2-${VERSION}"

patch -p0 \
  "${SRC}/util/grub-mkstandalone.c" \
  "tools/grub2/0001-mkstandalone-add-argument-fixed-time-to-override-mti.patch"
patch -p0 \
  "${SRC}/util/grub-mkrescue.c" \
  "tools/grub2/0002-mkrescue-add-argument-fixed-time-to-get-reproducible.patch"
patch -p0 \
  "${SRC}/Makefile.am" \
  "tools/grub2/0003-Makefile-use-FIXED_TIMESTAMP-for-mkstandalone-if-set.patch"
patch -p0 \
  "${SRC}/tests/util/grub-fs-tester.in" \
  "tools/grub2/0004-grub-fs-tester-use-SOURCE_DATE_EPOCH.patch"

(
cd "${SRC}"
debuild -b -uc -us
)

dpkg -i "${SCRATCH}"/grub-common_*.deb
dpkg -i "${SCRATCH}"/grub-pc-bin_*.deb
dpkg -i "${SCRATCH}"/grub-efi-amd64-bin_*.deb

grub-mkstandalone \
  --format=x86_64-efi \
  --output="${SCRATCH}/bootx64.efi.A" \
  --locales="" \
  --fonts="" \
  --fixed-time="${SOURCE_DATE_EPOCH}" \
  "boot/grub/grub.cfg=tools/grub.cfg"

grub-mkstandalone \
  --format=i386-pc \
  --output="${SCRATCH}/core.img.A" \
  --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
  --modules="linux normal iso9660 biosdisk search" \
  --locales="" \
  --fonts="" \
  --fixed-time="${SOURCE_DATE_EPOCH}" \
  "boot/grub/grub.cfg=tools/grub.cfg"

grub-mkstandalone \
  --format=x86_64-efi \
  --output="${SCRATCH}/bootx64.efi.B" \
  --locales="" \
  --fonts="" \
  --fixed-time="${SOURCE_DATE_EPOCH}" \
  "boot/grub/grub.cfg=tools/grub.cfg"

grub-mkstandalone \
  --format=i386-pc \
  --output="${SCRATCH}/core.img.B" \
  --install-modules="linux normal iso9660 biosdisk memdisk search tar ls" \
  --modules="linux normal iso9660 biosdisk search" \
  --locales="" \
  --fonts="" \
  --fixed-time="${SOURCE_DATE_EPOCH}" \
  "boot/grub/grub.cfg=tools/grub.cfg"

# TODO: move this check into a proper grub build test
diff \
  "${SCRATCH}/bootx64.efi.A" \
  "${SCRATCH}/bootx64.efi.B"

diff \
  "${SCRATCH}/core.img.A" \
  "${SCRATCH}/core.img.B"

cp "${SCRATCH}"/grub-common_*.deb "${BUILD_DIR}/"
cp "${SCRATCH}"/grub-pc-bin_*.deb "${BUILD_DIR}/"
cp "${SCRATCH}"/grub-efi-amd64-bin_*.deb "${BUILD_DIR}/"
