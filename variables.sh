#!/bin/sh
# Configuration for creation of the ISO image. This script is executed by
# create-iso.sh

set -x   # Print each command before executing it
set -e   # Exit immediately should a command fail
set -u   # Treat unset variables as an error and exit immediately

RELEASE=0.2.0   # Release version number
DATE=20200514   # Timestamp to use for version packages (`date +%Y%m%d`) matching slim-10.4
DIST=buster     # Debian distribution to base image on
ARCH=amd64      # Target architecture
SOURCE_DATE_EPOCH="$(date --utc --date="${DATE}" +%s)" # defined by reproducible-builds.org
BUILD="fasiac-koen-${RELEASE}"
WD="/tmp/${BUILD}"                  # Working directory to create the image
BUILD_DIR="/build"                  #
ISONAME="${BUILD}-${ARCH}.iso"      # Final name of the ISO image
TOOL_DIR=/tools                     # Location to install the tools
HOOK_DIR="${TOOL_DIR}/hooks"        # Hooks
PACKAGE_DIR="${TOOL_DIR}/packages"  # Packages

export RELEASE DATE DIST ARCH SOURCE_DATE_EPOCH WD ISONAME BUILD_DIR TOOL_DIR HOOK_DIR PACKAGE_DIR
