# FASIaC Key Operation Environment (FASIaC-KOEN) based on COEN

The FASIaC Key Operation Environment (FASIaC-KOEN) is a live operating system
consisting of:

- A custom Debian GNU/Linux Live CD
- Tools for [OpenSC](https://github.com/OpenSC/OpenSC/wiki) based smart cards / HSM
- Assorted utilities and scripts for key creation, signing and revocation of
  X509 certificates and GPG keys

## Reproducible ISO image to make your Keys more trustworthy
This **reproducible** ISO image provides a verifiable process to obtain the same artefact for every build. The sha512 hash of each build must match to verify that the identical artefact was created. The ISO image is able to reproduce itself. Having a reproducible build of the live ISO image increases the confidence in your keys. You should keep and store a non-modifiable copy together with your printed-out master private keys. That way you have a solid basis for a post-mortem analysis in case your keys are compromise. It will help you to check if the root cause for compromised keys was a compromised key handling environment.

### What are reproducible builds?
Quoted from https://reproducible-builds.org

> Reproducible builds are a set of software development practices that create a
verifiable path from human readable source code to the binary code used by
computers.
>
> Most aspects of software verification are done on source code, as that is what
humans can reasonably understand. But most of the time, computers require
software to be first built into a long string of numbers to be used. With
reproducible builds, multiple parties can redo this process independently and
ensure they all get exactly the same result. We can thus gain confidence that a
distributed binary code is indeed coming from a given source code.

## Packages from snapshot.debian.org
In fact, only an exact reproduction if binary artefacts is currently possible without lots of effort to individually patch upstream packages.
Note that reproducible debian builds still rely on snapshot.debian.org.
This archive exerts throttling to save operational costs.
That is why we use an apt-cacher-ng instance for the build process.
It could take some attempts before you are able to download all necessary
packages. If you want to remedy this situation, please consider to support
http://snapshot.debian.org/ with money, bandwidth or hardware.

## Support upstream reproducible builds
There is still a long road to a fully reproducible Linux distribution.
https://tests.reproducible-builds.org/debian/reproducible.html
To learn why reproducible builds are worthwhile, see
https://reproducible-builds.org/
While this project has a value in its own, it is much more worthwhile if you help making more software reproducible, generally.

## Acknowledgements
This project would be far more complicated to be achieved without:
- [COEN](https://github.com/iana-org/coen)
- The [Reproducible Builds](https://reproducible-builds.org/) project
- [Debian as trust anchor](https://wiki.debian.org/ReproducibleBuilds)
- [Debuerreotype](https://github.com/debuerreotype/debuerreotype) a
  reproducible, snapshot-based Debian rootfs builder (
  [License](https://github.com/debuerreotype/debuerreotype/blob/master/LICENSE))
- [The Amnesic Incognito Live System](https://tails.boum.org/index.en.html)
  ([License](https://tails.boum.org/doc/about/license/index.en.html))
- [OpenSC](https://github.com/OpenSC/OpenSC/wiki)

## Requirements to build the ISO image

Building the ISO image requires:

* Either [Docker](https://www.docker.com/) (the recommended Docker version is 19.03),
* or the built ISO image run as a live system itself.

## Building the ISO image

Execute the following commands to build the ISO image:

```
git clone https://git@fasiac.io/fasiac-koen.git && \
cd fasiac-koen && \
make all
```
* If you have an error executing `make all` as a non-root user, try to
execute `sudo make all`.

This will build a docker image with the proper environment to build the
ISO. Then it will run a container executing a shell script to build the ISO and
if the build succeeded, it will copy the resulting ISO into the host directory.

You can execute `make` command to see more options.

## Contributing

### If the build failed

Please file an issue with the error that is displayed in your terminal window.

### If the reproduction succeeded

Congrats for successfully reproducing the ISO image!

You can compute the SHA-512 checksum of the resulting ISO image by yourself:

```
sha512sum fasiac-koen-0.1.0-amd64.iso
```
or
```
shasum -a 512 fasiac-koen-0.1.0-amd64.iso
```

Then, comparing it with the following checksum:

```
5ee84148555e2d1fc4b195e24214aa73b3b2d55a5d3e089353ce6f0363072b5e127956ca22c04b227c2d968078131111d17bd99e130766e1a694f171bd772a33  fasiac-koen-0.1.0-amd64.iso
```

### If the reproduction failed

Please help us to improve it.
You can install `diffoscope` https://diffoscope.org/ and download the image
from:
https://git.fasiac.io/fasiac-koen/releases/tag/v0.1.0
and then compare it with your image executing the following command:

```
diffoscope \
  --text diffoscope.txt \
  path/to/public/fasiac-koen-0.1.0-amd64.iso \
  path/to/your/fasiac-koen-0.1.0-amd64.iso
```
Please file an issue report attaching the diffoscope.txt file.
