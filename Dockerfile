FROM debian:10.4-slim@sha256:91e111a5c5314bc443be24cf8c0d59f19ffad6b0ea8ef8f54aedd41b8203e3e1

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install \
    --no-install-recommends --yes \
    curl iproute2 iputils-ping net-tools

COPY tools/ /tools/
COPY ca-tools/ /ca-tools/
COPY variables.sh .

RUN . ./variables.sh \
    && rm -f /etc/apt/sources.list \
    && echo "deb http://fasiac-koen-apt-cacher-ng:3142/snapshot.debian.org/archive/debian/$(date --date "$DATE" '+%Y%m%dT%H%M%SZ') $DIST main" >> /etc/apt/sources.list \
    && echo "deb http://fasiac-koen-apt-cacher-ng:3142/snapshot.debian.org/archive/debian/$(date --date "$DATE" '+%Y%m%dT%H%M%SZ') "$DIST"-updates main" >> /etc/apt/sources.list \
    && echo "deb http://fasiac-koen-apt-cacher-ng:3142/snapshot.debian.org/archive/debian-security/$(date --date "$DATE" '+%Y%m%dT%H%M%SZ') "$DIST"/updates main" >> /etc/apt/sources.list

RUN apt-get update \
    -o Acquire::Check-Valid-Until=false \
    && apt-get install \
    -o Acquire::Check-Valid-Until=false \
    --no-install-recommends --yes \
    patch liblzo2-2 xorriso debootstrap \
    locales \
    && sed -i -e 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g' /etc/locale.gen \
    && locale-gen de_DE.UTF-8

ENV LANG de_DE.UTF-8
ENV LANGUAGE de_DE:de
ENV LC_ALL de_DE.UTF-8

RUN dpkg-reconfigure locales

RUN dpkg -i /tools/packages/squashfs-tools_4.3-8.0tails1_amd64.deb && \
    dpkg -i /tools/packages/debuerreotype_0.7-1_all.deb

RUN patch -p0 /usr/share/debuerreotype/scripts/.snapshot-url.sh /tools/debuerrotype-apt-cacher-ng.patch

COPY checksum.sh .
COPY README.md .
COPY LICENSE.md .
COPY create-iso.sh .
COPY SHA512SUMS .

RUN sha512sum -c SHA512SUMS

CMD ["/create-iso.sh"]
